from .utils.yuntongxun.sms import CCP
import logging
from celery_tasks.main import celery_app


logger = logging.getLogger("django")


@celery_app.task(name="send_sms_code")
def send_sms_code(expires, mobile, sms_code):
    """
    发送短信验证码
    :param  mobile: 手机号
    :param  sms_code: 验证码
    :param  expires: 有效期
    :return: None
    """
    try:
        ccp = CCP()
        result = ccp.send_template_sms(mobile, [sms_code, expires], 1)
    except Exception as e:
        logger.error("发送短信验证码异常[mobile:%s, message:%s]" % (mobile, e))

    # 返回数据
    if result == 0:
        logger.info("发送短信验证码正常[mobile:%s]" % mobile)
    else:
        logger.warning("发送短信验证码失败[mobile:%s]" % mobile)
