# 图片验证码保存到redis的有效期，单位：秒
IMAGE_CODE_REDIS_EXPIRES = 5*60

# 短信验证码保存redis的有效期
SMS_CODE_REDIS_EXPIRES = 5*60

# 是否在60内使用同一个号码请求发送短信记号
SEND_SMS_CODE_INTERVAL = 1*60