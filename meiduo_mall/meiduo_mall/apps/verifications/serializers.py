from rest_framework import serializers
from django_redis import get_redis_connection


class ImageCodeCheckSerializer(serializers.Serializer):
    """图片验证码校验序列化器"""
    image_code_id = serializers.UUIDField()
    text = serializers.CharField(max_length=4, min_length=4)

    def validate(self, attrs):
        image_code_id = attrs.get("image_code_id")
        text = attrs.get("text")

        con = get_redis_connection("verify_codes")
        real_text = con.get("img_%s" % image_code_id)

        if not real_text:
            raise serializers.ValidationError("image_code_id错误或者图片验证码已过期")

        # 为了防止用户通过恶意程序使用同一个图片验证码多次请求短信验证码，所以在取出real_text后立即删除
        # 还要在比较之前删除，如果比较失败了，直接raise报错，程序结束了，删除不了，如果比较成功，才能删除，可此时已经没有意义
        con.delete("img_%s" % image_code_id)

        # 从redis中取出的real_text是字节类型，需要转换成字符串类型来比较,都转换成小写字母
        real_text = real_text.decode()
        if real_text.lower() != text.lower():
            raise serializers.ValidationError("图片验证码错误")

        # 判断此手机号是否在60秒内发送给短信
        mobile = self.context["view"].kwargs["mobile"]
        send_flag = con.get("send_flag_%s" % mobile)
        if send_flag:
            raise serializers.ValidationError("短信发送请求过于频繁，一分钟内有发送过")

        return attrs
