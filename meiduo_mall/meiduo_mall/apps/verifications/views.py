from django.shortcuts import render
from django.http import HttpResponse
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.generics import GenericAPIView
from django_redis import get_redis_connection
import random
import logging

from celery_tasks.sms.tasks import send_sms_code
from meiduo_mall.libs.captcha.captcha import captcha
from meiduo_mall.utils.yuntongxun.sms import CCP
from . import constants
from .serializers import ImageCodeCheckSerializer


logger = logging.getLogger("django")


# 图片验证码 GET /image_codes/(?P<image_code_id>[\w-]+)/
class ImageCodeView(APIView):
    """图片验证码"""
    def get(self, request, image_code_id):
        # 获取并校验参数，这里的请求url中的正则已经做了校验
        # 生成图片验证码
        text, image = captcha.generate_captcha()
        print(text)

        # 已传来的image_code_id键将图片验证码的字符串保存到redis中,设置有效期5分钟
        con = get_redis_connection('verify_codes')
        con.setex("img_%s" % image_code_id, constants.IMAGE_CODE_REDIS_EXPIRES, text)

        # 返回图片验证码到前端
        return HttpResponse(image, content_type="image/jpg")


# 短信验证码 GET /sms_codes/(?P<mobile>1[3-9]\d{9})/?image_code_id=xxx&text=xxx
class SMSCodeView(GenericAPIView):
    """
    短信验证码
    传入参数：mobile, image_code_id, text
    """
    serializer_class = ImageCodeCheckSerializer

    def get(self, request, mobile):
        # 接收并校验参数，序列化器校验image_code_id和text,mobile在请求路由的正则已经校验过了
        serializer = self.get_serializer(data=request.query_params)
        serializer.is_valid(raise_exception=True)

        # 生成短信验证码
        sms_code = "%06d" % random.randint(0, 999999)

        # 保存短信验证码到redis中（以mobile为键），并保存是否在60内发送过短信的标志到redis，确保短信最快60秒才能发送一次
        con = get_redis_connection("verify_codes")
        pl = con.pipeline()  # 创建管道
        pl.setex("sms_%s" % mobile, constants.SMS_CODE_REDIS_EXPIRES, sms_code)
        pl.setex("send_flag_%s" % mobile, constants.SEND_SMS_CODE_INTERVAL, 1)
        pl.execute()  # 执行管道
        print(sms_code)

        # # 发送短信并返回发送状态
        # try:
        #     expires = constants.SMS_CODE_REDIS_EXPIRES // 60  # // 是取整数，/ 是浮点数，短信验证码有效期，单位分钟
        #     ccp = CCP()
        #     result = ccp.send_template_sms(mobile, [sms_code, expires], 1)  # 这里的1是模板编号，可写入常量内
        # except Exception as e:
        #     logger.error("发送短信验证码异常[mobile:%s, message:%s]" % (mobile, e))
        #     return Response({"message": "failed"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        #
        # # 返回数据
        # if result == 0:
        #     logger.info("发送短信验证码正常[mobile:%s]" % mobile)
        #     return Response({"message": "OK"}, )
        # else:
        #     logger.warning("发送短信验证码失败[mobile:%s]" % mobile)
        #     return Response({"message": "OK"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        # 使用celery发送短信验证码
        expires = constants.SMS_CODE_REDIS_EXPIRES // 60
        send_sms_code.delay(expires, mobile, sms_code)

        return Response({"message": "OK"})