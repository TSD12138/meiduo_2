from rest_framework_jwt.settings import api_settings

from aouth.models import OAuthQQUser
from aouth.utils import OAuthQQ
from users.serializers import serializers
from users.models import User


class OAuthQQUserSerializer(serializers.ModelSerializer):
    """保存第一次QQ登录用户序列化器"""
    sms_code = serializers.CharField(label="短信验证码", write_only=True)
    access_token = serializers.CharField(label="openid对应操作凭证", write_only=True)
    token = serializers.CharField(label="JWTtoken", read_only=True)
    # 重写mobile，原数据表模型中mobile属性是唯一的，但在这里不需要唯一，
    # 因为QQ绑定用户既可以是已存在的用户，也可以是新建一个用户,所以不能是唯一
    mobile = serializers.RegexField(label="手机号", regex=r'^1[3-9]\d{9}$')

    class Meta:
        model = User
        fields = ("mobile", "password", "sms_code", "access_token", "id", "username", "token")
        extra_kwargs = {
            "password": {
                "write_only": True,
                "min_length": 8,
                "max_length": 20,
                "error_messages": {
                    "min_length": "仅允许8-20个字符的密码",
                    "max_length": "仅允许8-20个字符的密码",
                }
            },
            "username": {
                # 因为原本的数据表模型username是前端既需要向后端传，处理后后端也需要向前端传的字段，
                # 但在这里绑定用户，前端是不需要向后端传的，所以要改写成 "read_only": True
                "read_only": True
            }
        }

    # 先校验access_token，取出openid
    # 根据手机号校验用户是否存在
    # 不存在的话新建用户，关联openid,签发JWTtoken返回
    # 存在的话，校验密码谁否正确，直接关联openid,签发JWTtoken返回
    def validate(self, attrs):
        # 校验access_token
        access_token = attrs["access_token"]
        openid = OAuthQQ.check_save_user_token(access_token)
        if not openid:
            raise serializers.ValidationError("无效的access_token")

        attrs["openid"] = openid   # 在后面的create方法中，validated_data需要用到openid,要往attrs字典中添加openid

        # 判断用户是否存在,校验密码
        mobile = attrs["mobile"]
        try:
            user = User.objects.get(mobile=mobile)
        except User.DoesNotExist:
            pass
        else:
            password = attrs["password"]
            if not user.check_password(password):
                raise serializers.ValidationError("用户密码错误")
            attrs["user"] = user

        return attrs

    def create(self, validated_data):
        mobile = validated_data.get("mobile")
        password = validated_data.get("password")
        openid = validated_data.get("openid")
        user = validated_data.get("user")  # 通过是否能取到user判断与openid绑定的用户是否存在
        if not user:  # 用户不存在，新建一个用户
            # 创建user,可使用数据表模型自带方法create_user,内部实现密码加密
            user = User.objects.create_user(username=mobile, password=password, mobile=mobile)

        # 再关联openid
        OAuthQQUser.objects.create(user=user, openid=openid)

        # 签发JWTtoken
        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

        payload = jwt_payload_handler(user)
        token = jwt_encode_handler(payload)

        user.token = token

        return user
