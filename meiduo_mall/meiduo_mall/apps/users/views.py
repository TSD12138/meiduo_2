from django.shortcuts import render
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.mixins import CreateModelMixin, UpdateModelMixin
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.generics import CreateAPIView, RetrieveAPIView, UpdateAPIView
from rest_framework.viewsets import GenericViewSet

from .models import User
from .serializers import CreateUserSerializer, UserDetailSerializer, EmailSerializer, AddressTitleSerializer, \
    UserAddressSerializer


# GET usernames/(?P<username>\w{5,20})/count/
class UserNameCountView(APIView):
    """判断用户名是否存在，即判断用户名数量"""
    def get(self, request, username):
        """数据库查询当前用户名数量"""
        count = User.objects.filter(username=username).count()

        data ={
            "username": username,
            "count": count
        }

        return Response(data)


# GET /mobiles/(?P<mobile>1[3-9]\d{9})/count/
class MobileCountView(APIView):
    """判断手机号是否存在，即判断手机号数量"""
    def get(self, request, mobile):
        """数据库查询当前用户手机号数量"""
        count = User.objects.filter(mobile=mobile).count()

        data ={
            "mobile": mobile,
            "count": count
        }

        return Response(data)


# 用户注册 POST /users/
class UserView(CreateAPIView):
    """
    用户注册
    传入参数：
        username, password, password2, sms_code, mobile, allow
    """
    serializer_class = CreateUserSerializer


# GET /user/   可直接继承自RetrieveAPIView，该视图提供get方法，继承自： GenericAPIView、RetrieveModelMixin
class UserDetailView(RetrieveAPIView):
    """用户详情个人中心"""
    serializer_class = UserDetailSerializer
    # 指明当前视图的权限,必须登录且必须完成JWT认证之后才能访问
    permission_classes = [IsAuthenticated]

    # GenericAPIView的get_object(self) 返回详情视图所需的模型类数据对象，默认使用lookup_field，也就是PK参数来过滤查询集queryset
    # 默认情况是在请求路由(/users/<pk>/)中获取pk,调用get_object方法来实现获取请求的用户对象
    # 所以需要重写get_object方法来得到请求的用户对象返回即可
    def get_object(self):
        """返回当前的请求用户对象user"""
        # APIView,在进行dispatch()分发前，会对请求进行身份认证、权限检查、流量控制,这里是进行JWTtoken认证
        # django中请求HttpRequest对象中有user属性，表明身份认证通过后当前请求的用户对象
        # 在类视图对象中，可以通过类视图对象的属性request获取当前请求对象，
        # 类视图对象还有kwargs属性来获取路径参数
        user = self.request.user
        return user


class EmailView(UpdateAPIView):
    """
    保存用户邮箱
    """
    permission_classes = [IsAuthenticated]
    serializer_class = EmailSerializer

    def get_object(self, *args, **kwargs):
        return self.request.user


# url(r'^emails/verification/$', views.VerifyEmailView.as_view()),
class VerifyEmailView(APIView):
    """
    邮箱验证
    """
    def get(self, request):
        # 获取token
        token = request.query_params.get('token')
        if not token:
            return Response({'message': '缺少token'}, status=status.HTTP_400_BAD_REQUEST)

        # 验证token
        user = User.check_verify_email_token(token)
        if user is None:
            return Response({'message': '链接信息无效'}, status=status.HTTP_400_BAD_REQUEST)
        else:
            user.email_active = True
            user.save()
            return Response({'message': 'OK'})


# 用户收货地址增删改查、设置默认地址、添加地址标题
# 使用视图集，6个action 动作（自定义两个）
# 新增继承扩展，删除是逻辑删除自己写，修改直接继承扩展，查自己写/因为返回格式要求，
class AddressViewSet(GenericViewSet, CreateModelMixin, UpdateModelMixin):
    """用户收货地址增删改查、设置默认地址、添加地址标题"""
    # 新增地址
    # POST / addresses /

    # 修改现有某个地址
    # POST / addresses / < pk > /

    # 查询现有所有地址
    # GET / addresses /

    # 删除现有某个地址
    # DELETE / addresses / < pk > /

    # 设置某个现有地址为默认地址
    # PUT / addresses / < pk > / status /

    # 设置某个现有地址的标题
    # PUT / addresses / < pk > / title /

    def get_queryset(self):
        # 返回当前请求用户的未逻辑删除的收货地址查询集,而不是数据库所有地址的查询集
        return self.request.user.addresses.filter(is_deleted=False)

    def get_serializer_class(self):
        if self.action == "title":
            return AddressTitleSerializer
        else:
            return UserAddressSerializer

    def create(self, request, *args, **kwargs):
        """新建用户"""
        # 新建之前需要判断一下是否已超过创建地址的最大数量
        count = request.user.addresses.count()
        if count >= 20:
            return Response({"message": "收货地址数量已达上限"}, status=status.HTTP_400_BAD_REQUEST)
        return super().create(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        """逻辑删除地址"""
        address = self.get_object()
        address.is_deleted = True
        address.save()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def list(self, request, *args, **kwargs):
        query_set = self.get_queryset()
        serializer_use = self.get_serializer_class()
        serializer = serializer_use(query_set, many=True)
        user = request.user
        return Response(
            {
                'user_id': user.id,
                'default_address_id': user.default_address_id,  # 不可以用user.default_address.id,因为针对一个没有设置任何默认地址的用户，user.default_address是None,None是没有id属性的，会报错
                'limit': 20,
                'addresses': serializer.data,
            }
        )

    @action(methods=['put'], detail=True)
    def status(self, request, pk=None):
        """设置默认地址"""
        address = self.get_object()
        user = request.user
        user.default_address = address
        user.save()
        return Response({'message': 'OK'}, status=status.HTTP_200_OK)

    @action(methods=['put'], detail=True)
    def title(self, request, pk=None):
        """设置地址标题"""
        data = request.data
        address = self.get_object()
        serializer_use = self.get_serializer_class()
        serializer = serializer_use(address, data=data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)

